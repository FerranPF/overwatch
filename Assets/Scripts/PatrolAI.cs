﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PatrolAI : MonoBehaviour {

	private NavMeshAgent agent;
	public GameObject[] destination;
	private int actualDir = 0;

	void Start(){
		agent = GetComponent<NavMeshAgent> ();
		agent.SetDestination(destination[actualDir].transform.position);
	}
	
	void Update () {
		if (!agent.pathPending && agent.remainingDistance < 0.5f) {
			NextPos ();
		}
		agent.SetDestination(destination[actualDir].transform.position);
	}

	void NextPos(){
		if (actualDir == destination.Length-1) {
			actualDir = 0;
		} else {
			actualDir++;
		}
	}
}
